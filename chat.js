const net = require('net');

const clients = {};

const server = net.createServer( socket => {
    let addr = socket.remoteAddress + ":" + socket.remotePort;
    clients[addr] = socket;
    socket.on("data", data => {
        Object.keys(clients).forEach( client => {
            //console.log(`registered ${client} local ${addr}`);
            if (client !== addr)
                clients[client].write(data.toString());
        });
    });
    socket.on("close", () => {
        delete clients[addr];
    });
});

server.listen(65000);
console.log("Chat server listening on port 65000");